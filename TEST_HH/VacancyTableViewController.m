//
//  VacancyTableViewController.m
//  TEST_HH
//
//  Created by Nick Korn on 07.09.15.
//  Copyright (c) 2015 Nick Korn. All rights reserved.
//

#import "VacancyTableViewController.h"

#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"

#define VACANCY_TABLE_VIEW_CELL_HEIGHT 48

@interface VacancyTableViewCell : UITableViewCell

@property (nonatomic, strong) UILabel *vacancyNameLabel;
@property (nonatomic, strong) UILabel *companyNameLabel;
@property (nonatomic, strong) UIImageView *companyLogoImage;
@property (nonatomic, strong) UILabel *salaryLabel;
@property (nonatomic, strong) UITextView *vacancyDescription;

@end

@implementation VacancyTableViewCell

@end

@interface VacancyTableViewController ()


@property (nonatomic, strong) NSMutableArray *vacancyArray;
@property (nonatomic) NSUInteger allDataPages;
@property (nonatomic) NSUInteger lastDataPage;
@property (nonatomic) NSInteger selectedRow;
@property (nonatomic) NSInteger selectedCellHeight;
@property (nonatomic, strong) NSString *vacancyDescription;

@end

@implementation VacancyTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.lastDataPage = 0;
    self.selectedRow = -1;
    
    self.vacancyArray = [NSMutableArray new];
    [self downloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return self.vacancyArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.selectedRow == indexPath.row ? VACANCY_TABLE_VIEW_CELL_HEIGHT + self.selectedCellHeight : VACANCY_TABLE_VIEW_CELL_HEIGHT;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIndentifier = @"VacancyTableViewCell";
    //VacancyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier forIndexPath:indexPath];
    VacancyTableViewCell *cell = (VacancyTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    if (!cell) {
        cell = [[VacancyTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIndentifier];
        
        CGRect frame = cell.contentView.bounds;
        frame.size.width = self.tableView.frame.size.width - 48;
        frame.origin.x = 48;
        frame.size.height = 12;

        cell.salaryLabel = [[UILabel alloc] initWithFrame:frame];
        [cell addSubview:cell.salaryLabel];
        cell.salaryLabel.font = [UIFont boldSystemFontOfSize:10];
        cell.salaryLabel.textColor = [UIColor whiteColor];
        cell.salaryLabel.backgroundColor = [UIColor grayColor];
        cell.salaryLabel.textAlignment = NSTextAlignmentLeft;
        cell.salaryLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;

        frame.origin.y += frame.size.height;
        frame.size.height = 24;
        
        cell.vacancyNameLabel = [[UILabel alloc] initWithFrame:frame];
        [cell addSubview:cell.vacancyNameLabel];
        cell.vacancyNameLabel.font = [UIFont systemFontOfSize:10];
        cell.vacancyNameLabel.textColor = [UIColor grayColor];
        cell.vacancyNameLabel.textAlignment = NSTextAlignmentLeft;
        cell.vacancyNameLabel.numberOfLines = 3;
        cell.vacancyNameLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        
        frame.origin.y += frame.size.height;
        frame.size.height = 12;
        
        cell.companyNameLabel = [[UILabel alloc] initWithFrame:frame];
        [cell addSubview:cell.companyNameLabel];
        cell.companyNameLabel.font = [UIFont boldSystemFontOfSize:10];
        cell.companyNameLabel.textColor = [UIColor grayColor];
        cell.companyNameLabel.textAlignment = NSTextAlignmentLeft;
        cell.companyNameLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;

        frame.origin.y += frame.size.height;
        frame.size.height = 0;
        cell.vacancyDescription = [[UITextView alloc] initWithFrame:frame];
        [cell addSubview:cell.vacancyDescription];
        cell.vacancyDescription.font = [UIFont boldSystemFontOfSize:10];
        cell.vacancyDescription.textColor = [UIColor grayColor];
        cell.vacancyDescription.textAlignment = NSTextAlignmentLeft;
        cell.vacancyDescription.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        cell.vacancyDescription.editable = NO;
        cell.vacancyDescription.scrollEnabled = NO;
        cell.vacancyDescription.userInteractionEnabled = NO;

        frame.origin.x = frame.origin.y = 4;
        frame.size.width = frame.size.height = 40;
        cell.companyLogoImage = [[UIImageView alloc] initWithFrame:frame];
        [cell addSubview:cell.companyLogoImage];
        
    }
    
    // Configure the cell...
    NSDictionary *vacancyDict = [self.vacancyArray objectAtIndex:indexPath.row];
    cell.companyNameLabel.text = [[vacancyDict objectForKey:@"employer"] objectForKey:@"name"];

    cell.vacancyNameLabel.text = [vacancyDict objectForKey:@"name"];

    NSDictionary *salaryDict = [vacancyDict objectForKey:@"salary"];
    if ([salaryDict isKindOfClass:[NSNull class]]) {
        cell.salaryLabel.backgroundColor = [UIColor grayColor];
        cell.salaryLabel.text = @"● Зарплата не указана";
    }
    else {
        NSInteger salaryFrom = [[salaryDict objectForKey:@"from"] isKindOfClass:[NSNumber class]] ? [[salaryDict objectForKey:@"from"] integerValue] : 0;
        NSInteger salaryTo = [[salaryDict objectForKey:@"to"] isKindOfClass:[NSNumber class]] ? [[salaryDict objectForKey:@"to"] integerValue] : 0;
        cell.salaryLabel.backgroundColor = [UIColor colorWithRed:0.5 green:1 blue:0.5 alpha:1];
        cell.salaryLabel.text = [NSString stringWithFormat:@"● %@%@%@ %@",
                                 salaryFrom > 0 ? [salaryDict objectForKey:@"from"] : @"",
                                 salaryFrom > 0 && salaryTo > 0 ? @"-" : @"",
                                 salaryTo > 0 ? [salaryDict objectForKey:@"to"] : @"",
                                 [salaryDict objectForKey:@"currency"]];
    }
    
    NSDictionary *logoDict = [[vacancyDict objectForKey:@"employer"] objectForKey:@"logo_urls"];
    if ([logoDict isKindOfClass:[NSDictionary class]]) {
        NSURL *url = [NSURL URLWithString:[logoDict objectForKey:@"90"]];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        UIImage *placeholderImage = [UIImage imageNamed:@"rateStarGreen.png"];
        
        __weak VacancyTableViewCell *weakCell = cell;
        
        [cell.companyLogoImage setImageWithURLRequest:request
                                     placeholderImage:placeholderImage
                                              success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                                  weakCell.companyLogoImage.image = image;
                                                  [weakCell setNeedsLayout];
                                              } failure:nil];
    }
    else [cell.companyLogoImage setImage:[UIImage imageNamed:@"rateStarGreen.png"]];
    
    if (self.selectedRow == indexPath.row) {
        cell.vacancyDescription.hidden = NO;
        cell.vacancyDescription.text = self.vacancyDescription;
        [cell.vacancyDescription sizeToFit];
        self.selectedCellHeight = cell.vacancyDescription.frame.size.height;
    }
    else {
        cell.vacancyDescription.hidden = YES;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.selectedRow == indexPath.row) {
        self.selectedRow = -1;
        [self.tableView reloadData];
        return;
    }
    self.selectedRow = indexPath.row;
    NSDictionary *vacancyDict = [self.vacancyArray objectAtIndex:indexPath.row];
    NSString *urlString = [vacancyDict objectForKey:@"url"];
    [self downloadDescriptionWithUrlString:urlString];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - scroll delegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if ((int)self.tableView.contentOffset.y ==  (int)self.tableView.contentSize.height - (int)self.tableView.frame.size.height) {
        [self downloadData];
    }
}

#pragma mark - Data Provide

- (void)downloadData
{
    NSString *urlString = @"https://api.hh.ru/vacancies";
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    [manager GET:urlString
      parameters:@{@"page": [NSNumber numberWithInteger:self.lastDataPage],
                   @"per_page": @20}
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *itemsArray = [responseObject objectForKey:@"items"];
        self.lastDataPage = [[responseObject objectForKey:@"page"] integerValue] + 1;
        self.allDataPages = [[responseObject objectForKey:@"pages"] integerValue];
        if (itemsArray && [itemsArray isKindOfClass:[NSArray class]]) {
            for (id object in itemsArray) {
                [self.vacancyArray addObject:object];
            }
        }
        [self.tableView reloadData];
        NSLog(@"DATA_RECIEVED_COUNT:%d PAGE:%d PAGES_QTY:%d", self.vacancyArray.count, self.lastDataPage, self.allDataPages);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error Loading Data"
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
    }];
}

- (void)downloadDescriptionWithUrlString:(NSString *)urlString
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager GET:urlString
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             self.vacancyDescription = [self stringRemoveTagsOfString:[responseObject objectForKey:@"description"]];
             [self.tableView reloadData];
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error Loading Data"
                                                                 message:[error localizedDescription]
                                                                delegate:nil
                                                       cancelButtonTitle:@"Ok"
                                                       otherButtonTitles:nil];
             [alertView show];
         }];
}

- (NSString *)stringRemoveTagsOfString:(NSString *)string
{
    NSRange range;
    while ((range = [string rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        string = [string stringByReplacingCharactersInRange:range withString:@""];
    return string;
}

@end
