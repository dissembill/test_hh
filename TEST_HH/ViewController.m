//
//  ViewController.m
//  TEST_HH
//
//  Created by Nick Korn on 07.09.15.
//  Copyright (c) 2015 Nick Korn. All rights reserved.
//

#import "ViewController.h"

#import "VacancyTableViewController.h"

@interface ViewController ()

@property (nonatomic, strong) VacancyTableViewController *vacancyTableViewController;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    [self createViews];
}

- (void)createViews
{
    CGRect frame;
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hh-black.png"]];
    [self.view addSubview:imageView];
    frame = imageView.frame;
    frame.size.width /= 2;
    frame.size.height /= 2;
    frame.origin.x = (self.view.frame.size.width - frame.size.width) / 2;
    frame.origin.y = frame.origin.x;
    imageView.frame = frame;
    
    self.vacancyTableViewController = [[VacancyTableViewController alloc] initWithNibName:nil bundle:nil];
    frame = self.view.bounds;
    frame.origin.y = 20;
    frame.size.height -= frame.origin.y;
    frame.origin.x += frame.size.width;
    self.vacancyTableViewController.view.frame = frame;
    [self.view addSubview:self.vacancyTableViewController.view];
    [self.vacancyTableViewController.tableView reloadData];
    frame.origin.x = 0;
    [UIView animateWithDuration:1
                          delay:1
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         self.vacancyTableViewController.view.frame = frame;
                     } completion:^(BOOL finished) {
                     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
